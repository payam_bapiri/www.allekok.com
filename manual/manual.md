# چۆنیەتی بەکارهێنانی ئاڵەکۆک
ئاڵەکۆک، شوێنێکە بۆ خوێندنەوەی شێعری کوردی. هەروەها کۆمەڵێک ئامێریشی هەیە کە یارمەتی‌و دەدات تا شێعری مەبەست‌تان ئاسان‌تر وەدۆزن، وەیخوێنن، پێداچوونەوەی بەسەردا بکەن و بۆ هاوڕێکان‌تانی بنێرن.  
لەم لاپەڕەدا بە تەسەلی باس لەم چوار بەشە گرینگە دەکەیین.  
- ١. دۆزینەوەی شێعر
- ٢. خوێندنەوەی شێعر
- ٣. پێداچوونەوەی شێعر
- ٤. بڵاوکردنەوەی شێعر

## ١. دۆزینەوەی شێعر
بە دوو جۆر دەتوانن ئەو شێعرەی مەبەست‌تانە بیدۆزنەوە:  
- بۆخۆتان بەدووی شێعرەکەدا بگەڕێن.
- بەشی گەڕانی ئاڵەکۆک بەکار بهێنن.

### بۆخۆتان بەدووی شێعرەکەدا بگەڕێن.
ئەگەر کات‌و هەیە، باشترین ڕێ ئەوەیە کە لە لاپەڕەی یەکەمی ماڵپەری ئاڵەکۆک را دەست پێ بکەن، لەسەر شاعیری ئەو شێعرەی بۆی دەگەڕێن، کرتە بکەن، دوایە لە بەشی بەرهەمەکان دا، ئەم کتێبەی ئەم شێعرەی تێدایە، هەڵبژێرن و لە کۆتاییش دا بەدوای سەرناوی شێعری مەبەست‌تان بگەڕێن هەتا دەی‌دۆزنەوە.  
ئاگاداری: ئامێرێک کە دەتوانێ بۆ دۆزینەوەی سەرناوی شێعر یارمەتی‌و بدا، "بەڕیزکردنی شێعرەکان لە ئاڕا"یە کە شێعرەکان بە ئەلفوبێ لەڕوی یەکەم پیتیان‌، ڕیز دەکات‌و ئێوە ئاسا‌ن‌تر دەتوانن شێعرەکەو وەبینن.

### بەشی گەڕانی ئاڵەکۆک بەکار بهێنن.
ئەگەر لەسەر <i class='material-icons'>search</i> کرتە بکەن کە لە گۆشەی سەرەوە لای چەپە، بەشی گەڕان دەهێنرێت. لێرەدا دەتوانن بۆو وشە، یان سەرناو یان هەربەشێک‌تر لەشێعر کە مەبەست‌تانە بگەڕێن. تەنانەت بۆ بەعزە شێعرێک دەتوانن بەدوای ڕێکەوتی نووسینی شێعریش‌دا بگەڕێن.  
ئاگاداری: لەکاتی گەڕان‌دا بەخەیاڵی ئاسوودە بنووسن‌و زۆر تامەزرۆی نووسینی ڕێنووسی دروست مەبن. چوونکی:  
- لە بەشی گەڕان‌دا هیچ جیاوازیەک نییە بەینی ("وو" و "و")، ("یی" و "ی")، ("لل" و "ل")، ("مم" و "م")، ("ل" و "ڵ")، ("و" و "ۆ")، ("ـە" و "هـ")، ("ر" و "ڕ") و زۆری تر. بەم جۆرە تەنانەت ئەگەر تەختەکلیلی کوردیش‌و نەبێت دەتوانن شێعری مەبەست‌تان وەدۆزن. بۆ نموونە هیچ جیاوازیەک بەینی ئەم سێ ڕێنووسەی خوارەوە نییەو هەر سێکیان دەتوانن بەکار بهێنن:  
- ئەرێ ئەی بە زولف و ڕوخسار
- ئەری ئەی بە زوڵف و روخسار
- ئه ری ئه ی بە زوولف و روخسار  
تکایە بە خەیاڵی ئاسوودە بگەڕێن و متمانەو هەبێ کە ئاڵەکۆک شێعری مەبەست‌تان دەدۆزێتەوە. ئەگەریش گەڕانەکەو هیچ ئەنجامێکی نەبوو، ڕێنووسەکەو بگۆڕن یان بەشێکی تری شێعرەکە(وشەیەکی تر، بەیتێکی تر، ...) بۆ گەڕان بەکار بهێنن.  

## ٢. خوێندنەوەی شێعر
کۆمەڵێک ئامێرمان بۆ یارمەتی‌دانی ئێوە لە خوێندنەوەی شێعردا ئامادە کردوە.  
بەشی ئامێرەکان، ئەم بەشەیە کە لە وێنەی خوارەوەدا نیشان دراوە. کاری ئامێرەکان بە پێی ژمارە لە ژێر وێنەکە شی کراوەتەوە.  
![ئامێرەکانی ئاڵەکۆک](toolbar.png "ئامێرەکانی ئاڵەکۆک")
- ١. ئەمە خەتی شێعر، گەورەتر دەکات.  
- ٢. ئەمە خەتی شێعر، چووک‌تر دەکاتەوە.  
- ٣. کۆپی کردنی شێعر.  
- ٤. نیشان‌کردنی شێعر. ئەگەر لەسەر ئەم دوکمەیە کرتە بکەن، ئەو
شێعرە نیشانە دەکرێ و لەسەر کامپیوتێڕەکەتان پاشەکەوت دەکرێ و
لەوەی بەدواوە هەر کات لەسەر ئەم کامپیوتێڕە سەردانی ئاڵەکۆک
بکەن، شێعرە نیشان کراوەکانتان لە سەرەوە لای چەپ (<i class='material-icons'>bookmark</i>) بۆ دەهێنێت.  
ئەگەر لەمەو پێش شێعرێک‌و نیشانە کردووە، بۆ لابردنی نیشانەکە دەتوانن بچنە ناو لاپەڕەی شێعر و لەسەر ئەم دوکمە کرتە بکەن.  
- ٥. ئەم بەشە بۆخۆی پێک‌هاتووە لە چەند ئامێر. ئەو بەشە داگری ئەم ئامێرانەیە کە کەمتر لە ئامێرەکانی‌تر بەکار دێن هەر بۆیە بۆ وەی کە پێش بە قەرەباڵغی بگیرێ، لێرە داندراون. ئامێرەکان پێک‌هاتوون لە:  
	- ١.٥. لینک بۆ وەشانەکانی‌تری ئەم شێعرە لەسەر ماڵپەڕەکانی‌تر. ئەم وەشانانە لەوانەیە لەگەڵ وەشانی ئاڵەکۆک جیاوازیان هەبێت، لەوانەشە یەکسان بن.  
(ئەم تایبەتمەندیە بۆ هەموو شێعرەکان نییە و بۆ ئەوانەی کە هەشە لەوانەیە لینکەکە هەڵە بێت.)  
	- ٢.٥. وەشانی تێکستی ئەم شێعرە، دەتوانن دایگرن و بەکاری بێنن.
	- ٣.٥. گۆڕینی ئەلفوبێ بۆ لاتین.  
(بۆ ئەم ئامێرە لە کۆدەکانی <a target="_blank" href="<?php echo _R; ?>kurdi-nus/kurdi-nus-central-kurdish.html">پەڵک کوردی‌نووس</a> مان کەڵک وەرگرتووە. بەڵام لەوانەیە ئەم گۆڕانکارییە بێ‌هەڵە نەبێت. لەکاتی بەکارهێنانی دا وشیار بن.)  
	- ٤.٥. دروست کردنی فەرهەنگێک بۆ وشەکانی ناو دەقی ئەم شێعرە. ئەم فەرهەنگە لە تەنیشت دەقی شێعرەکە نیشان دەدرێت.  
	- ٥.٥. گەڕان بۆ واتای وشە لە "تەوار"دا.  
(<a target="_blank" href="<?php echo _R; ?>tewar/">تەوار</a>، ئامێرێکە بۆ گەڕان بەدوای واتای وشە لە چەند فەرهەنگ‌دا؛ وەکوو: هەنبانەبۆرینە، خاڵ، کاوە و... .)  

## ٣. پێداچوونەوەی شێعر
بەشی پێداچوونەوە، بەشی ژێرەوەی هەر شێعرەیە.  
لەوانەیە شێعرێک‌و زۆر بەلاوە جوان بێت یان هیچ خۆشی‌و لێ نەیە، لە بیرو بێ بیروڕای ئێوە هەمیشە بەنرخەو ئێوە ئازادن بیروبۆچوونی خۆتان لە خوارەوەی هەر شێعرێک‌دا بنووسن.  
شایەدیش لە کاتی خوێندنەوەی شێعردا، ڕەخنەیەکی تەکنیکی یان ئەدەبی‌ یان هەر جۆرێکی‌ترو پێ شک بێت. لەو کاتەش دا دەتوانن ڕەخنەکەتان لە بەشی ژێرەوەی شێعردا بنووسن و شی کەنەوە، تا لە زووترین کات‌دا لە لایەن ئێمە پێداچوونەوەی بەسەردا بکرێت.  
سەرنج بدەن نووسراوەکەتان بۆ هەمیشە لەسەر ئاڵەکۆک دەمێنێتەوە و هیچ کات بێ‌ئیزنی ئێوە پاک ناکرێتەوە. تەنیا یەک مەرج هەیە بۆ پاک کردنەوەی بیروڕا، ئەویش ئەوەیە کە بیروڕاکەتان بەنێوی کەسێکی ڕاستەقینەی‌تر نووسیبێ‌و ئەو داوامان لێ بکات پاکی کەینەوە.  
کەوایە نیگەران مەبن‌و بە ئاسوودەیی بیر و ڕای خۆتان بنووسن.  

## ٤. بڵاوکردنەوەی شێعر
لە لاپەڕەی هەر شێعرێک‌دا دەتوانن دەقی ئەم شێعرە کۆپی بکەن‌و بۆ هاوڕێکانتانی بنێرن. یان دەتوانن لەسەر دوکمەی "کۆپی کردن" لەسەرەوەی هەر شێعرەوە کرتە بکەن و تەواوی دەقی شێعرەکە کۆپی بکەن.   
(سەرنج بدەن، دوکمەی "کۆپی کردن" سەرناو و پەراوێزی شێعرەکە کۆپی ناکات، تەنیا دەقی شێعرەکە کۆپی دەکات.)  
ڕێگایەکی دیکەش ئەوەیە کە لینکی ئەو لاپەڕەی ئێستا خەریکی سەیر کردنین، بۆ هاوڕێکانتان بنێرن. کە بە بڕوای ئێمە ڕێگایەکی باش‌ترە لە ڕێگای سەرەوە. چون ئاڵەکۆک، بێجگە لەوەی کە کۆمەڵە ئامێرێکی زۆری هەیە بۆ یارمەتی‌دانی خوێنەر، شێعرەکان دروست‌تر و جوان‌تر و بە خێرایەکی زیاتر نیشان دەدا. بەڵام لە کۆتایی‌دا ئیختیار بەخۆتانە.  

